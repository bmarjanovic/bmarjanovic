# About me :wave:

Hi, I'm Bojan and I am a [Senior Backend Engineer](https://handbook.gitlab.com/job-families/engineering/development/backend/senior/) on the [Cells Infrastructure Team](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/tenant-scale/cells-infrastructure/) team at GitLab.

# :computer: Tech Stack:
![Ruby](https://img.shields.io/badge/ruby-%23CC342D.svg?style=for-the-badge&logo=ruby&logoColor=white) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white) ![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white)


# Availablility :alarm_clock:

I live in Berlin, Germany 🇩🇪, and I'm usually online from 9 AM to 5 PM CET Monday through Friday. However, you can always find me somewhere around the world. I enjoy traveling and exploring new places, cultures, and food. 

Typically, I aim to synchronize my schedule with daylight hours instead of following the clock. After waking up and going through my morning routines, I grab a coffee ☕, then dive into my emails and To-Do list.

My work is organized into distinct blocks, each dedicated to specific tasks, such as code reviews, focused efforts on deliverables, continuous improvement initiatives, and personal development time.

## Preferred engagement style :unicorn:

To get my attention, `@` mention me in issues/MRs which I monitor through TODOs and emails. For urgent matters, pinging me directly on Slack is best.
